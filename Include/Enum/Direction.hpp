#ifndef LIBTCOD_DIRECTION_HPP
#define LIBTCOD_DIRECTION_HPP

namespace Doryen
{
    enum class Direction : short
    {
        NORTH_WEST,
        NORTH,
        NORTH_EAST,
        WEST,
        NONE,
        EAST,
        SOUTH_WEST,
        SOUTH,
        SOUTH_EAST
    };
}

#endif //LIBTCOD_DIRECTION_HPP
